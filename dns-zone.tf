data "google_dns_managed_zone" "root" {
  name    = var.dns_zone_name
  project = var.project
}

resource "google_dns_record_set" "control_plane_load_balancer" {
  count = length(keys(var.service_ports))

  name = "${keys(var.service_ports)[count.index]}.${data.google_dns_managed_zone.root.dns_name}"
  type = "A"
  ttl  = 60

  managed_zone = data.google_dns_managed_zone.root.name

  rrdatas = [
    "${google_compute_address.ip_address[count.index].address}",
  ]

  project = var.project
}
