data "google_compute_image" "control_plane" {
  family  = "control-plane"
  project = var.project
}

resource "google_compute_instance" "control_plane" {
  count = var.instances

  name         = "control-${count.index + 1}"
  machine_type = "n1-standard-1"
  project      = var.project
  zone         = var.zone

  tags = ["consul"]

  boot_disk {
    initialize_params {
      image = data.google_compute_image.control_plane.self_link
    }
  }

  network_interface {
    network = "default"

    access_config {
    }
  }

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }

  connection {
    type        = "ssh"
    host        = self.network_interface.0.access_config.0.nat_ip
    user        = var.ssh_user
    private_key = file(var.ssh_private_key)
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /tmp/tls",
      "sudo mkdir -p /etc/tls /etc/consul /etc/vault /etc/nomad",
    ]
  }

  provisioner "file" {
    destination = "/tmp/tls/key.pem"
    content     = tls_private_key.server[count.index].private_key_pem
  }

  provisioner "file" {
    destination = "/tmp/tls/ca.pem"
    content     = vault_pki_secret_backend_sign.server[count.index].ca_chain[length(vault_pki_secret_backend_sign.server[count.index].ca_chain) - 1]
  }

  provisioner "file" {
    destination = "/tmp/tls/cert.pem"
    content = join("\n", flatten([
      vault_pki_secret_backend_sign.server[count.index].certificate,
      vault_pki_secret_backend_sign.server[count.index].ca_chain,
    ]))
  }

  provisioner "file" {
    destination = "/tmp"
    source      = "${path.module}/files"
  }

  provisioner "file" {
    destination = "/tmp/files/consul/datacenter.json"
    content = templatefile("${path.module}/templates/consul/datacenter.json", {
      datacenter = var.datacenter
    })
  }

  provisioner "file" {
    destination = "/tmp/files/consul/gossip.json"
    content = templatefile("${path.module}/templates/consul/gossip.json", {
      gossip_key = random_id.consul_gossip_key.b64_std
    })
  }

  provisioner "file" {
    destination = "/tmp/files/consul/retry-join.json"
    content = templatefile("${path.module}/templates/consul/retry-join.json", {
      project = var.project
    })
  }

  // provisioner "file" {
  //   destination  = "/tmp/files/consul/connect.json"
  //   content      = templatefile("${path.module}/templates/consul/connect.json", {
  //     private_key = jsonencode(var.intermediate_private_key_pem)
  //     root_cert   = jsonencode(var.intermediate_certificate_pem)
  //   })
  // }

  provisioner "file" {
    destination = "/tmp/files/nomad/server.hcl"
    content = templatefile("${path.module}/templates/nomad/server.hcl", {
      gossip_key = random_id.nomad_gossip_key.b64_std
    })
  }

  provisioner "file" {
    destination = "/tmp/files/nomad/datacenter.hcl"
    content = templatefile("${path.module}/templates/nomad/datacenter.hcl", {
      datacenter = var.datacenter
    })
  }

  provisioner "file" {
    destination = "/tmp/files/nomad/region.hcl"
    content = templatefile("${path.module}/templates/nomad/region.hcl", {
      region = var.region
    })
  }

  provisioner "file" {
    destination = "/tmp/environment"
    content = file("${path.module}/files/environment")
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/tls/* /etc/tls",
      "sudo mv /tmp/files/consul/* /etc/consul",
      "sudo mv /tmp/files/vault/* /etc/vault",
      "sudo mv /tmp/files/nomad/* /etc/nomad",
      "sudo mv /tmp/files/systemd/* /lib/systemd/system",
      "sudo mv /tmp/environment /etc/environment",
      "sudo systemctl daemon-reload",
      "sudo systemctl enable consul.path",
      "sudo systemctl start consul.path",
      "sudo systemctl enable nomad.path",
      "sudo systemctl start nomad.path",
      "sudo systemctl enable vault.path",
      "sudo systemctl start vault.path",
    ]
  }

  provisioner "remote-exec" {
    when = destroy
    scripts = [
      "${path.module}/scripts/consul-leave.sh",
    ]
  }
}
