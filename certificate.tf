resource "vault_pki_secret_backend_sign" "server" {
  count = var.instances

  backend = var.pki_backend_path
  name    = var.pki_role_name

  csr         = tls_cert_request.server[count.index].cert_request_pem
  common_name = tls_cert_request.server[count.index].subject[0].common_name
}

resource "tls_private_key" "server" {
  count = var.instances

  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "tls_cert_request" "server" {
  count = var.instances

  key_algorithm   = "ECDSA"
  private_key_pem = tls_private_key.server[count.index].private_key_pem

  subject {
    common_name = "control-plane"
  }

  dns_names = flatten([
    "server.${var.datacenter}.consul",
    "server.${var.region}.nomad",
    "localhost",
    formatlist("%s.service.consul", ["vault", "active.vault", "standby.vault"]),
    formatlist("%s.%s", ["consul", "vault", "nomad"], data.google_dns_managed_zone.root.dns_name)
  ])

  ip_addresses = [
    "127.0.0.1",
  ]
}
