storage "consul" {
  address       = "127.0.0.1:8501"
  scheme        = "https"
  tls_ca_file   = "/etc/tls/ca.pem"
  tls_key_file  = "/etc/tls/key.pem"
  tls_cert_file = "/etc/tls/cert.pem"
}
