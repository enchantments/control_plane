listener "tcp" {
  address            = "0.0.0.0:8200"
  tls_client_ca_file = "/etc/tls/ca.pem"
  tls_key_file       = "/etc/tls/key.pem"
  tls_cert_file      = "/etc/tls/cert.pem"
}
