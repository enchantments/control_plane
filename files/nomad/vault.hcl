vault {
  enabled          = true
  ca_file          = "/etc/tls/ca.pem"
  cert_file        = "/etc/tls/cert.pem"
  key_file         = "/etc/tls/key.pem"
  address          = "https://localhost:8200"
  create_from_role = "nomad-cluster"
}
