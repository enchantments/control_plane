resource "random_id" "consul_gossip_key" {
  byte_length = 16
}

resource "random_id" "nomad_gossip_key" {
  byte_length = 16
}
