output "public_ipv4_addresses" {
  value = google_compute_instance.control_plane.*.network_interface.0.access_config.0.nat_ip
}

output "instance_names" {
  value = google_compute_instance.control_plane.*.name
}

output "consul_gossip_key" {
  value = random_id.consul_gossip_key.b64_std
}

output "consul_address" {
  value = "https://consul.${data.google_dns_managed_zone.root.dns_name}:8501"
}

output "vault_address" {
  value = "https://vault.${data.google_dns_managed_zone.root.dns_name}:8200"
}

output "nomad_address" {
  value = "https://nomad.${data.google_dns_managed_zone.root.dns_name}:4646"
}
