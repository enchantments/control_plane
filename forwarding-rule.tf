resource "google_compute_forwarding_rule" "control_plane" {
  count   = length(keys(var.service_ports))
  name    = keys(var.service_ports)[count.index]
  project = var.project
  region  = var.gcp_region

  target                = google_compute_target_pool.control_plane.self_link
  load_balancing_scheme = "EXTERNAL"
  ip_protocol           = "TCP"
  port_range            = lookup(var.service_ports, keys(var.service_ports)[count.index], 0)
  ip_address            = google_compute_address.ip_address[count.index].address
}

variable "service_ports" {
  type = map
  default = {
    consul = 8501
    vault  = 8200
    nomad  = 4646
  }
}
