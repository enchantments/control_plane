resource "google_compute_address" "ip_address" {
  count = length(keys(var.service_ports))

  name    = keys(var.service_ports)[count.index]
  project = var.project
  region  = var.gcp_region
}
