variable "instances" {
  default = 3
}

variable "datacenter" {}
variable "region" {}
variable "ssh_user" {}
variable "ssh_private_key" {}
variable "project" {}
variable "gcp_region" {}
variable "zone" {}
variable "pki_vault_address" {}
variable "pki_vault_token" {}
variable "pki_backend_path" {}
variable "pki_role_name" {}
variable "dns_zone_name" {}

variable "consul_version" {
  default = ""
}

variable "vault_version" {
  default = "1.4.0"
}

variable "nomad_version" {
  default = ""
}
