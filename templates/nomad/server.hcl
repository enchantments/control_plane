server {
  enabled          = true
  bootstrap_expect = 3
  raft_protocol    = 3
  encrypt          = "${gossip_key}"
}
