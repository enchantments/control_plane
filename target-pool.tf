resource "google_compute_target_pool" "control_plane" {
  name = "control-plane"

  instances = google_compute_instance.control_plane.*.self_link
  project   = var.project
  region    = var.gcp_region
}
